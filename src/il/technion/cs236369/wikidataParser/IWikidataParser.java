package il.technion.cs236369.wikidataParser;

import java.nio.file.Path;
import java.util.Set;

import org.json.simple.JSONArray;

public interface IWikidataParser {

	JSONArray find(Path dump, Set<String> baseItems, int dosThreshold);  
	
}
