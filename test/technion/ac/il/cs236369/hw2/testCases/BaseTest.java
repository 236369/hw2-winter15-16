package technion.ac.il.cs236369.hw2.testCases;

import il.technion.cs236369.wikidataParser.IWikidataParser;

public class BaseTest {
	protected IWikidataParser parser;
	public BaseTest() {
		try {
			parser = (IWikidataParser) Class.forName("il.technion.cs236369.wikidataParser.WikidataParser").newInstance();
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException e) {
			e.printStackTrace();
			System.err.println("Couldn't find 'il.technion.cs236369.wikidataParser.IWikidataParser' class");
			System.err.println("Make sure you have this class defined and implements the IWikidataParser interface");
		}
	}
  
}
