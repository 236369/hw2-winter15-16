package technion.ac.il.cs236369.hw2.testCases;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashSet;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class SimilarItem {
	public String id;
	public String label;
	public String similarToId;
	public String similarToLabel;
	int dos;
	public HashSet<Similarity> similaritiesSet = new HashSet<Similarity>();
	  
	public SimilarItem(String id, String label, String similarToId, String similarToLabel, int dos) {
		this.id = id;
		this.label = label;
		this.similarToId = similarToId;
		this.similarToLabel = similarToLabel;
		this.dos = dos;
	}
	
	public void setSimilarities(JSONArray arr) {
		for (int i=0; i < arr.size() ; i++) {
			JSONObject simObject = (JSONObject) arr.get(i);
			Similarity sim = new Similarity(
					(String) simObject.get("propertyId"),
					(String) simObject.get("propertyLabel"),
					(String) simObject.get("similarValue"),
					(String) simObject.get("similarValueLabel"));
			similaritiesSet.add(sim);
		}
	}

	public boolean isExpectedEqualToActual(Object obj) {
		if (!(obj instanceof SimilarItem)) return false;
		SimilarItem s = (SimilarItem)obj;
		
		if (!id.equals(s.id)) return false;
		if (label != null) {
			if (!label.equals(s.label)) return false;
		}
		if (!similarToId.equals(s.similarToId)) return false;
		if (similarToLabel != null) {
			if (!similarToLabel.equals(s.similarToLabel)) return false;
		}
		if (dos != s.dos) return false;
		
		for (Similarity p1: similaritiesSet) {
			boolean hasMatchInEqual = false;
			for (Similarity p2: s.similaritiesSet) {
				 if (p1.isExpectedEqualToActual(p2)) {
					 hasMatchInEqual = true;
					 break;
				 }
			}
			if (!hasMatchInEqual) {
				return false;
			}
		}
		
		return true;
	}
	
	static public boolean equals(String expcted, JSONArray actual) {

		HashSet<SimilarItem> similarItemSetExpcted = new HashSet<SimilarItem>();
		HashSet<SimilarItem> similarItemSetActual = new HashSet<SimilarItem>();
		
		Object expctedObj= JSONValue.parse(expcted);
		JSONArray expctedJsonArray =(JSONArray)expctedObj;
		assertNotNull(expctedJsonArray);
		
		assertNotNull(actual);
		
		assertEquals(expctedJsonArray.size(), actual.size());
		
		for (int i =0; i <expctedJsonArray.size(); i++) {
			JSONObject j = (JSONObject) expctedJsonArray.get(i);
			assertNotNull(j);
			SimilarItem s = new SimilarItem((String) j.get("id"), 
					(String)j.get("label"), 
					(String)j.get("similarToId"), 
					(String)j.get("similarToLabel"), 
					(int)((long)j.get("dos")));
			JSONArray jsonSimilarities = (JSONArray)j.get("similarities");
			s.setSimilarities(jsonSimilarities);	
			similarItemSetExpcted.add(s);
		}
		
		for (int i =0; i < actual.size(); i++) {
			JSONObject j = (JSONObject) actual.get(i);
			assertNotNull(j);	
			SimilarItem s = new SimilarItem((String) j.get("id"), 
					(String)j.get("label"), 
					(String)j.get("similarToId"), 
					(String)j.get("similarToLabel"), 
					(int)j.get("dos"));
			JSONArray jsonSimilarities = (JSONArray)j.get("similarities");
			s.setSimilarities(jsonSimilarities);
			similarItemSetActual.add(s);
		}
			
		for (SimilarItem s1: similarItemSetExpcted) {
			boolean hasMatchInEqual = false;
			for (SimilarItem s2 : similarItemSetActual) {
				 if (s1.isExpectedEqualToActual(s2)) {
					 hasMatchInEqual = true;
					 break;
				 }
			}
			if (!hasMatchInEqual) {
				return false;
			}
		}
		return true;
	}
	
}
