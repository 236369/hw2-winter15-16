package technion.ac.il.cs236369.hw2.testCases;

public class Similarity {
	public String propertyId;
	public String propertyLabel;
	public String similarValue;
	public String similarValueLabel;
	
	public Similarity(String propertyId, String propertyLabel, String similarValue, String similarValueLabel) {
		this.propertyId = propertyId;
		this.propertyLabel = propertyLabel;
		this.similarValue = similarValue;
		this.similarValueLabel = similarValueLabel;
	}  

	public boolean isExpectedEqualToActual(Object obj) {
		
		if (!(obj instanceof Similarity)) return false;
		Similarity s = (Similarity)obj;
		
		if (!propertyId.equals(s.propertyId)) return false;
		if (propertyLabel != null) {
			if (!propertyLabel.equals(s.propertyLabel)) return false;
		}
		if (similarValue != null) {
			if (!similarValue.equals(s.similarValue)) return false;
		}
		if (similarValueLabel != null) {
			if (!similarValueLabel.equals(s.similarValueLabel)) return false;
		}
		
		return true;
	}
	
}
